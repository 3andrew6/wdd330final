const links = [
    {
        label: "Pokedex - Challenge #2",
        url:"pokedex/pokedex.html"
    },
    {
        label: "Week1 notes",
        url:"week01/week1notes.html"
    },
    {
        label: "Week2 notes",
        url:"week02/week02notes.html"
    },
    {
        label: "Week2 assignment",
        url:"week02/week02.html"
    },
    {
        label: "Week3 notes",
        url:"week03/week3notes.html"
    },
    {
        label: "Week3 assignment",
        url:"week03/ArrayCardio.html"
    },
    {
        label: "Week4 notes",
        url:"week04/week4notes.html"
    },
    {
        label: "Week4 assignment",
        url:"week04/TicTacTo.html"
    },
    {
        label: "Week5 notes",
        url:"week05/week5notes.html"
    },
    {
        label: "Week5 assignment",
        url:"week05/index.html"
    },
    {
        label: "Todo Challenge",
        url:"todo/todo.html"
    },
    {
        label: "Week7 assignment",
        url:"week07/index.html"
    },
    {
        label: "Week7 notes",
        url:"week07/week07notes.html"
    },
    {
        label: "Week8 assignment",
        url:"week08/week08assignment.html"
    },
    {
        label: "Week8 notes",
        url:"week08/week08notes.html"
    },
    {
        label: "Week9 assignment",
        url:"week09/index-START.html"
    },
    {
        label: "Week9 notes",
        url:"week09/week09notes.html"
    },
    {
        label: "Week10 assignment",
        url:"week10/W10teamactivity.html"
    },
    {
        label: "Week10 notes",
        url:"week10/week10notes.html"
    },
    {
        label: "Week11 assignment",
        url:"week11/client/week11.html"
    }
        

]

function main(){
    var ul = document.getElementById("weeks");

    for (var i = 0; i < links.length; i++){
        var li = document.createElement('li');
        var aTag = document.createElement('a');

        aTag.textContent = links[i].label;
        aTag.setAttribute('href', links[i].url);
        li.appendChild(aTag)
        ul.appendChild(li);
    }
}

main();