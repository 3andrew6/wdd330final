class todo{
    constructor (content, completed, id) {
        this.content = content;
        this.completed = completed;
        this.id = id
    }
    
    setComplete() { 
        if(this.completed){
            this.completed = false;
        } 
        else{
            this.completed = true;
        }
    }
}

export {todo};
