import {todo} from './ToDos.js';
import {loadArray, saveArray} from "./ls.js";

// create list array
let toDoListArray = [];

//load list array from memory and display
window.addEventListener("load", () => {
    toDoListArray = loadArray();
    showToDoList();
});

//add listner for addToDo button
document.getElementById("submit").addEventListener("click", addTodo);

//add complete button listener to all tasks
var elementsComplete = document.getElementsByClassName("complete");
for (var i = 0; i < elementsComplete.length; i++) {
    elementsComplete[i].addEventListener("click", changeStatus);    
}

//add remove button listener to all tasks
var elementsRemove = document.getElementsByClassName("remove");
for (var i = 0; i < elementsRemove.length; i++) {
    elementsRemove[i].addEventListener("click", remove);    
}

//add task item from input into array
function addTodo(){
    //add content and timestamp to each task
    if (document.getElementById("inputText").value != '') {
        let newContent = document.getElementById("inputText").value;
        var today = new Date();
        var newId = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        toDoListArray.push(new todo(newContent, false, newId));     
    }
    
    //clear input for new task
    clearInput();
    saveArray(toDoListArray);
    showToDoList();
}

//clear input field
function clearInput(){
    document.getElementById("inputText").value = "";
}

//change status of task from true to false or false to true
function changeStatus(i){
    toDoListArray[i].setComplete();
    showToDoList();
    saveArray(toDoListArray);
}
window.changeStatus = changeStatus;

//remove task from list of tasks
function remove(i){
    toDoListArray.splice(i, 1);
    showToDoList();
    saveArray(toDoListArray);
}
window.remove = remove;

//change display between all, active, and completed
var display = 'ALL';
function changeDisplay(displayType){
    display = displayType;
    showToDoList();
}
window.changeDisplay = changeDisplay;

//show to do list
function showToDoList() {
    saveArray(toDoListArray);
    const toDoList = document.getElementById("toDoList");
    toDoList.innerHTML = "";
    renderToDoList(toDoListArray, toDoList);
  }
  
  //append all tasks to parent ul
function renderToDoList(toDos, parent) {
    var i = 0;
    toDos.forEach(toDo => {
        let li = renderToDo(toDo, i);
        if (li) {
            parent.appendChild(li);
        }
        i++
    });
}
  
  //render each task
function renderToDo(toDo, i) {

    //render tasks based on if they are active or completed and what display is active
    if (display == 'ACTIVE' && toDo.completed == true) {
        return;
    }

    if (display == 'COMPLETED' && toDo.completed == false) {
        return;
    }
    const item = document.createElement("li");

    var displayComplete = " ";
    if (toDo.completed == true) {
        displayComplete = "X";
    }

    item.innerHTML = ` 
        <div class="toDo">
            <button onClick="changeStatus(${i})" style="width:20px;height:20px;";>${displayComplete}</button>
            <p class="task">${toDo.content}</p>
            <button class="removeB" onClick="remove(${i})">X</button>
        </div>
        `;
    return item;
}